import React, { Fragment, useContext } from "react";
import Avatar from "@material-ui/core/Avatar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import ReportContext from "../../context/reports/reportContext";

import EqualizerIcon from "@material-ui/icons/Equalizer";

import Barchart from "./Barchart";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.info.light,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Report = () => {
  const classes = useStyles();

  const reportContext = useContext(ReportContext);

  const {
    reports_questionnaires_by_project,
    answers,
    subfactors,
  } = reportContext;

  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <CssBaseline />
        <Grid container justify="center">
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <EqualizerIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Reporte del Proyecto
            </Typography>
          </div>
        </Grid>
        <Grid container justify="center" spacing={4}>
          <Grid item sm={12}>
            <Typography
              component="h2"
              variant="h6"
              color="primary"
              gutterBottom
            >
              Gráfico de Barras
            </Typography>
          </Grid>
          <Grid item sm={6}>
            <Barchart data={reports_questionnaires_by_project} />
          </Grid>
          <Grid item sm={6}>
            <Typography variant="h4">Detalle de los resultados</Typography>{" "}
            <br />
            {answers
              ? answers.map((factor, factorIndex) => (
                  <Fragment>
                    <Fragment>
                      <Typography variant="h5">
                        Factor {factorIndex + 1}
                      </Typography>
                      <Typography variant="h6">
                        <strong>Subatributo:</strong> {"" + factor.factor_name}
                        {"          "}
                        <strong>Puntaje:</strong>{" "}
                        {"     " + factor.factor_score}
                      </Typography>
                    </Fragment>
                    <Fragment>
                      <Typography variant="h5">Subfactores</Typography>
                      <Typography variant="h6">
                        <strong>Pregunta:</strong> {"" + factor.subfactor_name}
                        {"          "}
                        <strong>Puntaje:</strong>{" "}
                        {"     " + factor.subfactor_score}
                      </Typography>
                    </Fragment>
                  </Fragment>
                ))
              : null}
          </Grid>
          <Grid item justify="center" sm={12}>
            <Typography variant="h3" color="primary" gutterBottom>
              Resultados de la evaluación
            </Typography>
            {reports_questionnaires_by_project
              ? reports_questionnaires_by_project.map((questionnaire) => (
                  <Grid key={questionnaire.id_questionnaire} item>
                    <Typography variant="h6">
                      {"El cuestionario " +
                        questionnaire.name +
                        " tuvo un nivel de pertenencia de usabilidad del " +
                        questionnaire.score +
                        " sobre 100. "}
                      {
                        "Por lo tanto, la usabilidad del sitio web evaluado tiene un nivel de adecuación: "
                      }

                      {questionnaire.score < 40
                        ? "Inadecuado"
                        : questionnaire.score > 40 && questionnaire.score <= 60
                        ? "Razonable"
                        : questionnaire.score > 60 && questionnaire.score <= 80
                        ? "Adecuado"
                        : "Muy adecuado"}
                    </Typography>
                  </Grid>
                ))
              : null}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Report;
