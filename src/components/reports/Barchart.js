import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

function createData(name, score) {
  return { name, score };
}

function getData(data) {
  var list = [];
  if (data) {
    data.map((row) => {
      return list.push(createData(row.name, row.score));
    });
    console.log("List", list);
    return list;
  } else {
    return null;
  }
}

export default function Chart(props) {
  return (
    <React.Fragment>
      <ResponsiveContainer width="100%" height={500}>
        <BarChart
          width={500}
          height={500}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
          data={getData(props.data)}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis type="number" domain={[0, 100]} />
          <Tooltip />
          <Legend />
          <Bar dataKey="score" fill="#8884d8" />
        </BarChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}
