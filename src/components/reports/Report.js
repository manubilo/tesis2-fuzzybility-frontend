import React, { Fragment, useContext } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";

import ProjectContext from "../../context/project/projectContext";
import AuthContext from "../../context/auth/authContext";
import ReportContext from "../../context/reports/reportContext";

import EqualizerIcon from "@material-ui/icons/Equalizer";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.info.light,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Report = () => {
  const classes = useStyles();

  const projectContext = useContext(ProjectContext);
  const authContext = useContext(AuthContext);
  const reportContext = useContext(ReportContext);

  const { projects } = projectContext;
  const { user } = authContext;
  const {
    listQuestionnairesByProject,
    getAnswers,
    reports_questionnaires_by_project,
  } = reportContext;

  const onClickVerReporte = (id_project) => {
    listQuestionnairesByProject({
      id_user: user.id_user,
      id_project: id_project,
    });

    console.log(
      "reports_questionnaires_by_project[0]",
      reports_questionnaires_by_project[0]
    );

    getAnswers({
      id_project: id_project,
      id_questionnaire: reports_questionnaires_by_project[0].id_questionnaire,
    });
  };

  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <CssBaseline />
        <Grid container justify="center">
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <EqualizerIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Reportes
            </Typography>
          </div>
        </Grid>
        <Grid container justify="center" spacing={2}>
          <Fragment>
            {projects
              ? projects.map((project) => (
                  <Grid key={project.project.id_project} item>
                    {project.project.status === "Completo" &&
                    project.role === 2 ? (
                      <Card className={classes.root}>
                        <CardContent>
                          <Typography
                            style={{
                              backgroundColor: "#64b5f6",
                            }}
                            variant="h6"
                          >
                            {project.project.title}
                          </Typography>
                          <Typography
                            className={classes.title}
                            color="textSecondary"
                            gutterBottom
                          >
                            Fecha de Creación: {project.creation_date}
                          </Typography>
                          <Typography
                            className={classes.title}
                            color="textSecondary"
                            gutterBottom
                          >
                            Estado: {project.project.status}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button
                            onClick={() =>
                              onClickVerReporte(project.project.id_project)
                            }
                            component={Link}
                            to={"/reportsChart"}
                            size="small"
                          >
                            Ver cuestionarios
                          </Button>
                        </CardActions>
                      </Card>
                    ) : null}
                  </Grid>
                ))
              : null}
          </Fragment>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Report;
