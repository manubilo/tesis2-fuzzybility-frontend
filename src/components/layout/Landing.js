import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const Landing = () => {
  const classes = useStyles();
  return (
    <Container component="main" maxWidth="lg">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography
          style={{ alignText: "start", margin: 40 }}
          variant="h3"
          color="primary"
        >
          Fuzzybility
        </Typography>
        <Typography style={{ margin: 40 }} variant="h4" color="primary">
          Medición del nivel de Usabilidad de sitios web de Gobierno Electrónico
          usando Lógica Difusa
        </Typography>
        <Button variant="contained" color="primary" href="/about">
          Conoce Más
        </Button>
      </div>
    </Container>
  );
};

export default Landing;
