import React, { useContext } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import { Button } from "@material-ui/core";

import AuthContext from "../../context/auth/authContext";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    marginLeft: theme.spacing(5),
    display: "flex",
    flexDirection: "column",
    alignItems: "start",
  },
}));

const About = () => {
  const classes = useStyles();

  const authContext = useContext(AuthContext);
  const { isAuthenticated } = authContext;

  return (
    <Container component="main">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography variant="h4">Proyecto de Tesis 2</Typography>
        <Typography variant="h6">Título:</Typography>
        <Typography variant="body1">
          Implementación de un sistema de información para medir el nivel de
          usabilidad de sistemas web que brindan servicios de gobierno
          electrónico usando lógica difusa
        </Typography>
        <Typography variant="h6">Alumno:</Typography>
        <Typography variant="body1">Manuel Delgado Alba</Typography>

        <Typography variant="h6">Asesores:</Typography>
        <Typography variant="body1">
          Dr. Freddy Alberto Paz Espinoza, Dr. Manuel Francisco Tupia Anticona
        </Typography>
      </div>

      <div className={classes.paper}>
        <Typography variant="h4">¿Cómo comenzar?</Typography>

        <Typography variant="body1">
          1. Crear un cuestionario de medición de usabilidad seleccionando los
          subatributos.
        </Typography>

        <Typography variant="body1">
          2. Crear un proyecto de medición de un sitio web que brinde servicios
          de gobierno electrónico.
        </Typography>

        <Typography variant="body1">
          3. Realizar la evaluación de usabilidad del sitio web seleccionado
          utilizando uno de los cuestionarios creados
        </Typography>

        <Typography variant="body1">
          4. Visualizar reportes y estadísticas de la evaluación por proyecto de
          medición
        </Typography>
      </div>

      {isAuthenticated ? (
        <div className={classes.paper} style={{ alignItems: "center" }}>
          <Button href="/newquestionnaire" variant="contained" color="primary">
            Crear un cuestionario
          </Button>
        </div>
      ) : (
        <div className={classes.paper} style={{ alignItems: "center" }}>
          <Button href="/signin" variant="contained" color="primary">
            Iniciar Sesión
          </Button>
        </div>
      )}

      <div className={classes.paper}>
        <Typography variant="h4">
          ¿Cómo se calcula el nivel de usabilidad?
        </Typography>

        <Typography variant="body1">
          1. Una vez culminada la evaluación de un cuestionario de usabilidad
          por todos los usuarios, se identifica el mínimo, máximo y promedio de
          las respuestas a cada una de las preguntas.
        </Typography>

        <Typography variant="body1">
          2. Estos datos son evaluados en la fórmula:{" "}
          <strong>
            {" "}
            Subatributo(X) = 0.25 * min + 0.5 * promedio + 0.25 * max{" "}
          </strong>
        </Typography>

        <Typography variant="body1">
          3. Por cada subatributo, se realiza un promedio simple de los valores
          obtenidos, de cada pregunta, mediante la fórmula del punto anterior
        </Typography>

        <Typography variant="body1">
          4. El valor del nivel de usabilidad es calculado realizando un
          promedio simple de los valores obtenidos de los subatributos a partir
          del punto anterior
        </Typography>
      </div>
    </Container>
  );
};

export default About;
