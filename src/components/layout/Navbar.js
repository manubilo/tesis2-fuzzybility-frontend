import React, { Fragment, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";

import AuthContext from "../../context/auth/authContext";
import ProjectContext from "../../context/project/projectContext";
import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const Navbar = () => {
  const classes = useStyles();
  const projectContext = useContext(ProjectContext);
  const questionnaireContext = useContext(QuestionnaireContext);
  const authContext = useContext(AuthContext);

  const { isAuthenticated, logout, user } = authContext;
  const { listProjects } = projectContext;
  const { listQuestionnairesByUser } = questionnaireContext;

  const onProjects = () => {
    listProjects(user.id_user);
  };

  const onQuestionnaire = () => {
    listQuestionnairesByUser(user.id_user);
  };
  const onReports = () => {
    listProjects(user.id_user);
  };

  const onLogout = () => {
    logout();
  };

  const authLinks = (
    <Fragment>
      <Button component={Link} to={"/about"} color="inherit">
        Sobre el proyecto
      </Button>
      <Button
        onClick={onProjects}
        component={Link}
        to={"/projects"}
        color="inherit"
      >
        Proyectos
      </Button>
      <Button
        onClick={onQuestionnaire}
        component={Link}
        to={"/questionnaires"}
        color="inherit"
      >
        Cuestionarios
      </Button>
      <Button
        onClick={onReports}
        component={Link}
        to={"/reports"}
        color="inherit"
      >
        Reportes
      </Button>
      <Button onClick={onLogout} component={Link} to={"/"} color="inherit">
        Cerrar Sesión
      </Button>
    </Fragment>
  );

  const guestLinks = (
    <Fragment>
      <Button component={Link} to={"/about"} color="inherit">
        Sobre el proyecto
      </Button>
      <Button component={Link} to={"/signin"} color="inherit">
        Iniciar Sesión
      </Button>
    </Fragment>
  );

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Grid item xs={3}>
            <Button href="/" color="inherit">
              <Typography style={{ textTransform: "none" }} variant="h6">
                Fuzzybility
              </Typography>
            </Button>
          </Grid>

          <Grid item xs={9}>
            {isAuthenticated ? authLinks : guestLinks}
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Navbar;
