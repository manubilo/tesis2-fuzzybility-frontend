import React, { Fragment, useContext, useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";

import Grid from "@material-ui/core/Grid";

import AuthContext from "../../context/auth/authContext";
import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";
import ProjectContext from "../../context/project/projectContext";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const AnswerQuestionnaire = () => {
  const classes = useStyles();

  const authContext = useContext(AuthContext);
  const { user } = authContext;

  const questionnaireContext = useContext(QuestionnaireContext);
  const {
    questionnaire_to_answer,
    id_questionnaire,
    answerQuestionnaire,
  } = questionnaireContext;

  const projectContext = useContext(ProjectContext);
  const { id_project } = projectContext;

  const [formData, setFormData] = useState({
    id_project: id_project,
    id_questionnaire: id_questionnaire,
  });

  //CAMBIAR 5000
  //CAMBIAR 5000
  //CAMBIAR 5000
  //Enviar del back el numero de preguntas y colocar Array(questionnaire_to_answer.number_of_questions)
  const [questionAnswer, setQuestionAnswer] = useState(Array(5000).fill(1));

  const handleQuestionAnswerChange = (
    event,
    id_question,
    subattributeIndex,
    questionIndex
  ) => {
    console.log(id_question);
    const questionAnswerAux = [...questionAnswer];
    questionAnswerAux[id_question] = event.target.value;
    setQuestionAnswer(questionAnswerAux);

    //En el arreglo questionAnswer estan las respuestas elegidas
    //Las posiciones de cada respuesta coinciden con el id_question
    //Tengo que hacer que el arreglo questionAnswer tenga un tamaño de acuerdo al numero de preguntas
    //Y que cada respuesta se coloque en su casilla correspondiente
    //Por ejemplo, marco 2  en la primera pregunta del primer subatributo
    //Deberia ser: questionAnswer[0] = 2
    //Si marco 3 en la quinta pregunta del primer subatributo
    //Deberia ser: questionAnswer[4] = 3
    //Para luego solamente iterar ese arreglo y colocar las respuestas en el objeto formData
    //Que voy a enviar al back

    let fd = { ...formData };
    let qta = [...questionnaire_to_answer];
    console.log("fd", fd);
    if (!fd.subattribute_list) {
      fd.subattribute_list = qta;
    }
    fd.subattribute_list[subattributeIndex].questions_list[
      questionIndex
    ].answer = questionAnswer[id_question];

    fd.id_user = user.id_user;
    //fd.id_questionnaire = id_questionnaire;
    //fd.id_project = id_project;
    setFormData(fd);
    //question.statement_type = event.target.value;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    formData.subattribute_list.forEach((sub) => {
      sub.questions_list.forEach((question) => {
        let id_q = question.id_question;
        question.answer = questionAnswer[id_q];
      });
    });
    answerQuestionnaire(formData);
  };

  useEffect(() => {
    console.log("formData en use effect", formData);
    //eslint-disable-next-line
  }, [formData]);

  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <CssBaseline />
        <Grid container justify="center">
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>{" "}
            <Typography component="h1" variant="h5">
              Responder Cuestionario{" "}
            </Typography>{" "}
          </div>{" "}
        </Grid>
        <Grid container spacing={2}>
          <form onSubmit={onSubmit} className={classes.form} noValidate>
            {questionnaire_to_answer
              ? questionnaire_to_answer.map(
                  (subattribute, subattributeIndex) => (
                    <Grid key={subattribute.id_subattribute} item>
                      <Typography variant="h4">{subattribute.name}</Typography>
                      {subattribute.questions_list
                        ? subattribute.questions_list.map(
                            (question, questionIndex) => (
                              <Grid key={question.id_question} item>
                                <Typography variant="h6">
                                  Pregunta: {question.description}
                                </Typography>
                                <Fragment>
                                  <FormControl component="fieldset">
                                    <Grid container spacing={2}>
                                      <Grid item xs={4}>
                                        <FormLabel component="legend"></FormLabel>
                                      </Grid>

                                      <RadioGroup
                                        name={question.id_question.toString()}
                                        value={
                                          questionAnswer[question.id_question]
                                        }
                                        onChange={(e) =>
                                          handleQuestionAnswerChange(
                                            e,
                                            question.id_question,
                                            subattributeIndex,
                                            questionIndex
                                          )
                                        }
                                      >
                                        <Grid container>
                                          <Grid item>
                                            <FormControlLabel
                                              value="1"
                                              control={<Radio />}
                                              label="1"
                                            />
                                          </Grid>

                                          <Grid item>
                                            <FormControlLabel
                                              value="2"
                                              control={<Radio />}
                                              label="2"
                                            />
                                          </Grid>
                                          <Grid item>
                                            <FormControlLabel
                                              value="3"
                                              control={<Radio />}
                                              label="3"
                                            />
                                          </Grid>
                                          <Grid item>
                                            <FormControlLabel
                                              value="4"
                                              control={<Radio />}
                                              label="4"
                                            />
                                          </Grid>
                                          <Grid item>
                                            <FormControlLabel
                                              value="5"
                                              control={<Radio />}
                                              label="5"
                                            />
                                          </Grid>
                                        </Grid>
                                      </RadioGroup>
                                    </Grid>
                                  </FormControl>
                                </Fragment>
                              </Grid>
                            )
                          )
                        : null}
                    </Grid>
                  )
                )
              : null}
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Responder cuestionario{" "}
            </Button>
          </form>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default AnswerQuestionnaire;
