import React, { useContext, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import TextField from "@material-ui/core/TextField";

import AuthContext from "../../context/auth/authContext";
import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";
import ProjectContext from "../../context/project/projectContext";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const CommentQuestionnaire = () => {
  const classes = useStyles();

  const authContext = useContext(AuthContext);
  const { user } = authContext;

  const questionnaireContext = useContext(QuestionnaireContext);
  const { id_questionnaire, commentQuestionnaire } = questionnaireContext;

  const projectContext = useContext(ProjectContext);
  const { id_project } = projectContext;

  const [formData, setFormData] = useState(null);

  const handleComentarioPuntaje = (e) => {
    let fd = { ...formData };
    fd.id_user = user.id_user;
    fd.id_questionnaire = id_questionnaire;
    fd.id_project = id_project;
    fd.score_comment = e.target.value;
    setFormData(fd);
  };

  const handleComentarioMejora = (e) => {
    let fd = { ...formData };
    fd.improvement_comment = e.target.value;
    setFormData(fd);
  };

  const handleComentarioGeneral = (e) => {
    let fd = { ...formData };
    fd.general_comment = e.target.value;
    setFormData(fd);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    console.log("formData en onSubmit", formData);
    commentQuestionnaire(formData);
  };

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>{" "}
        <Typography component="h1" variant="h5">
          Comentar cuestionario{" "}
        </Typography>{" "}
      </div>{" "}
      <form onSubmit={onSubmit} className={classes.form} noValidate>
        <TextField
          onChange={handleComentarioPuntaje}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="questionnaire-title"
          label="Comentario sobre el puntaje obtenido"
        />
        <TextField
          onChange={handleComentarioMejora}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="questionnaire-title"
          label="Comentario sobre opciones de mejora del sitio web"
        />
        <TextField
          onChange={handleComentarioGeneral}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="questionnaire-title"
          label="Comentario en general"
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Comentar Cuestionario{" "}
        </Button>
      </form>
    </Container>
  );
};

export default CommentQuestionnaire;
