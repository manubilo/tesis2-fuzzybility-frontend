import React, { Fragment, useContext } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import FileCopyIcon from "@material-ui/icons/FileCopy";

import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";

import QuestionnaireCard from "./QuestionnaireCard";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.warning.light,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Questionnaires = () => {
  const classes = useStyles();

  const questionnaireContext = useContext(QuestionnaireContext);

  const { questionnaires_by_user } = questionnaireContext;

  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <Grid container justify="center">
          <CssBaseline />
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <FileCopyIcon />
            </Avatar>{" "}
            <Typography component="h1" variant="h5">
              Cuestionarios{" "}
            </Typography>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              component={Link}
              to={"/newquestionnaire"}
            >
              Nuevo Cuestionario{" "}
            </Button>
          </div>{" "}
        </Grid>
      </Grid>
      <Grid container justify="center" spacing={4}>
        <Fragment>
          {" "}
          {questionnaires_by_user
            ? questionnaires_by_user.map((questionnaire) => (
                <Grid key={questionnaire.id_questionnaire} item>
                  <QuestionnaireCard
                    key={questionnaire.id_questionnaire}
                    questionnaire={questionnaire}
                  />
                </Grid>
              ))
            : null}{" "}
        </Fragment>
      </Grid>
    </Grid>
  );
};

export default Questionnaires;
