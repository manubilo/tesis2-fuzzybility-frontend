import React, { Fragment, useContext, useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import FileCopyIcon from "@material-ui/icons/FileCopy";

import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Delete";

import AuthContext from "../../context/auth/authContext";
import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";

import { v4 as uuidv4 } from "uuid";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.warning.light,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  card: {
    padding: theme.spacing(3, 2, 2),
  },
}));

const NewQuestionnaire = () => {
  const classes = useStyles();

  const authContext = useContext(AuthContext);
  const { user } = authContext;

  const questionnaireContext = useContext(QuestionnaireContext);
  const { createQuestionnaire } = questionnaireContext;

  const [formData, setFormData] = useState(null);

  const [subattribute_list, setSubattributeList] = useState([]);

  useEffect(() => {
    //eslint-disable-next-line
  }, [subattribute_list, formData]);

  const handleTituloCuestionario = (e) => {
    let fd = { ...formData };
    fd.name = e.target.value;
    fd.id_user = user.id_user;
    fd.likert_number = 5;
    setFormData(fd);
  };

  const handleSubattributeName = (subattribute, e) => {
    let s = subattribute_list.find((s) => {
      return s.id_sub === subattribute.id_sub;
    });
    s.name = e.target.value;
  };

  const onClickAgregarSubatributo = () => {
    let auxSubList = [...subattribute_list];
    auxSubList.push({
      id_sub: uuidv4(),
      name: null,
      questions_list: [],
    });
    setSubattributeList(auxSubList);
  };

  const handleQuestionName = (subattributeIndex, e, questionIndex) => {
    let auxSubattributeList = [...subattribute_list];

    auxSubattributeList[subattributeIndex].questions_list[
      questionIndex
    ].description = e.target.value;

    setSubattributeList(auxSubattributeList);
  };

  const onClickAgregarPregunta = (subattributeIndex) => {
    console.log(subattributeIndex);
    let auxSubattributeList = [...subattribute_list];
    auxSubattributeList[subattributeIndex].questions_list.push({
      id_preg: uuidv4(),
      description: null,
      statement_type: 1,
    });

    setSubattributeList(auxSubattributeList);
  };

  const deleteQuestion = (subattribute, questionIndex) => {
    console.log("questionIndex", questionIndex);
    let auxSubattributeList = [...subattribute_list];

    let s = auxSubattributeList.find((s) => {
      return s.id_sub === subattribute.id_sub;
    });

    s.questions_list.splice(questionIndex, 1);

    setSubattributeList(auxSubattributeList);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    let fd = { ...formData };
    fd.subattribute_list = subattribute_list;
    setFormData(fd);
    console.log("subattribute_list en onSubmit", subattribute_list);
    console.log("formData en onSubmit", formData);
    createQuestionnaire(formData, subattribute_list);
  };

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <FileCopyIcon />
        </Avatar>{" "}
        <Typography component="h1" variant="h5">
          Crear Nuevo Cuestionario{" "}
        </Typography>{" "}
      </div>{" "}
      <form onSubmit={onSubmit} className={classes.form} noValidate>
        <TextField
          onChange={handleTituloCuestionario}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="questionnaire-title"
          label="Título del cuestionario"
        />
        <Grid
          container
          alignItems="flex-start"
          justify="flex-end"
          direction="row"
        >
          <Button
            onClick={() => onClickAgregarSubatributo()}
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Agregar subatributo{" "}
          </Button>
        </Grid>
        {subattribute_list
          ? subattribute_list.map((subattribute, subattributeIndex) => (
              <Card className={classes.root}>
                <CardContent>
                  <Fragment key={subattribute.id_sub}>
                    <Grid container spacing={2}>
                      <Grid item xs={9}>
                        <TextField
                          onChange={(e) =>
                            handleSubattributeName(subattribute, e)
                          }
                          variant="outlined"
                          margin="normal"
                          required
                          fullWidth
                          id="subattribute.id_sub"
                          label="Ingresar subatributo"
                          value={subattribute.name}
                        />
                      </Grid>
                      <Grid item xs={3}>
                        <Button
                          onClick={() => {
                            console.log("entre a delete icon");
                            console.log(subattribute);
                            setSubattributeList((subattribute_list) =>
                              subattribute_list.filter(
                                (x) => x.id_sub !== subattribute.id_sub
                              )
                            );
                          }}
                          variant="contained"
                          color="secondary"
                          className={classes.submit}
                        >
                          <DeleteIcon />
                        </Button>
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      alignItems="flex-start"
                      justify="flex-end"
                      direction="row"
                    >
                      <Button
                        onClick={() =>
                          onClickAgregarPregunta(subattributeIndex)
                        }
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                      >
                        Agregar pregunta{" "}
                      </Button>
                    </Grid>
                    {subattribute.questions_list
                      ? subattribute.questions_list.map(
                          (question, questionIndex) => (
                            <Card className={classes.root}>
                              <CardContent>
                                <Fragment>
                                  <Grid container spacing={2}>
                                    <Grid item xs={9}>
                                      <TextField
                                        onChange={(e) =>
                                          handleQuestionName(
                                            subattributeIndex,
                                            e,
                                            questionIndex
                                          )
                                        }
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="question.id_preg"
                                        label="Ingresar pregunta"
                                        value={question.description}
                                      />
                                    </Grid>
                                    <Grid item xs={3}>
                                      <Button
                                        onClick={() =>
                                          deleteQuestion(
                                            subattribute,
                                            questionIndex
                                          )
                                        }
                                        variant="contained"
                                        color="secondary"
                                        className={classes.submit}
                                      >
                                        <DeleteIcon />
                                      </Button>
                                    </Grid>
                                  </Grid>
                                </Fragment>
                              </CardContent>
                            </Card>
                          )
                        )
                      : null}
                  </Fragment>
                </CardContent>
              </Card>
            ))
          : null}
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Crear Cuestionario{" "}
        </Button>{" "}
      </form>{" "}
    </Container>
  );
};

export default NewQuestionnaire;
