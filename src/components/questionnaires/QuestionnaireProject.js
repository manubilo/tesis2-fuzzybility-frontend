import React, {Fragment, useContext} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import {Link} from "react-router-dom"
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import QuestionnaireCard from './QuestionnaireCard'

import QuestionnaireContext from '../../context/questionnaire/questionnaireContext'


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const QuestionnaireProject = ()  =>  {
  const classes = useStyles();

  const projectContext = useContext(ProjectContext);

  const { projects } = projectContext;



  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Proyectos
        </Typography>

        <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            component={Link} 
            to={'/newproject'}
          >
            Nuevo Proyecto
          </Button>

          <Fragment>
            {projects ? projects.map(project => (
              <ProjectCard key = {project.id_project} project = {project} />
            )) : null}
          </Fragment>
      </div>
    </Container>
  );
}

export default QuestionnaireProject;