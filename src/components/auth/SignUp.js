import React, { useContext, useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Swal from "sweetalert2";
import "date-fns";

import AuthContext from "../../context/auth/authContext";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Fuzzybility
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUp = () => {
  const classes = useStyles();

  const authContext = useContext(AuthContext);

  const { register } = authContext;

  const [user, setUser] = useState({
    username: "",
    password: "",
    first_name: "",
    last_name: "",
    email: "",
    birth_date: "",
    sex: null,
    highest_education: "",
    occupation: "",
    active_status: 1,
  });

  const {
    username,
    password,
    first_name,
    last_name,
    email,
    birth_date,
    sex,
    highest_education,
    occupation,
    active_status,
  } = user;

  const onChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const onSubmit = (e) => {
    e.preventDefault();

    let regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      username === "" ||
      password === "" ||
      first_name === "" ||
      last_name === "" ||
      email === ""
    ) {
      //setAlert('Por favor, ingrese todos los campos', 'danger');
      Swal.fire({
        title: "Error",
        text: "Por favor, ingrese todos los campos",
        icon: "error",
      });
    } else if (first_name.trim().length < 3) {
      Swal.fire({
        title: "Error",
        text: "Ingresar nombres de 3 caracteres como mínimo",
        icon: "error",
      });
    } else if (last_name.trim().length < 3) {
      Swal.fire({
        title: "Error",
        text: "Ingresar apellidos de 3 caracteres como mínimo",
        icon: "error",
      });
    } else if (!regexEmail.test(email)) {
      Swal.fire({
        title: "Error",
        text: "Ingresar un correo electrónico válido",
        icon: "error",
      });
    } else if (password.trimLeft().length < 4) {
      Swal.fire({
        title: "Error",
        text: "Ingresar una contraseña de 4 caracteres como mínimo",
        icon: "error",
      });
    } else {
      register({
        username,
        password,
        first_name,
        last_name,
        email,
        birth_date,
        sex,
        highest_education,
        occupation,
        active_status,
      });
    }
  };

  useEffect(() => {
    //eslint-disable-next-line
  }, [user]);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Registro de usuario
        </Typography>
        <form onSubmit={onSubmit} className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                name="first_name"
                variant="outlined"
                required
                fullWidth
                id="first_name"
                label="Nombres"
                value={first_name}
                onChange={onChange}
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="last_name"
                label="Apellidos"
                name="last_name"
                value={last_name}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="email"
                label="Correo Electrónico"
                id="email"
                value={email}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Nombre de Usuario"
                name="username"
                value={username}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                type="password"
                name="password"
                label="Contraseña"
                id="password"
                value={password}
                onChange={onChange}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Registrarme
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/signin" variant="body2">
                ¿Ya tiene una cuenta? Inicie sesión
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default SignUp;
