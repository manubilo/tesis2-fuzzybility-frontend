import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const ProjectCard = ({ project }) => {
  const classes = useStyles();

  const { title, creation_date, status } = project;

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="h6">{title}</Typography>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          Fecha de Creación: {creation_date}
        </Typography>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          Estado: {status}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Ver proyecto</Button>
      </CardActions>
    </Card>
  );
};

export default ProjectCard;
