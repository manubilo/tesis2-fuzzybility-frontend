import React, { Fragment, useContext, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";

import ProjectContext from "../../context/project/projectContext";
import AuthContext from "../../context/auth/authContext";
import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";

import Grid from "@material-ui/core/Grid";
import FolderIcon from "@material-ui/icons/Folder";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.success.light,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Projects = () => {
  const classes = useStyles();

  const projectContext = useContext(ProjectContext);
  const authContext = useContext(AuthContext);
  const questionnaireContext = useContext(QuestionnaireContext);

  const {
    projects,
    getUsers,
    project_questionnaires,
    getProjectQuestionnaires,
    id_project,
    setSelectedProject,
  } = projectContext;
  const { user } = authContext;
  const { listQuestionnairesByUser } = questionnaireContext;

  useEffect(() => {}, [projects, project_questionnaires, id_project]);

  const onClickNewProject = () => {
    getUsers();
    listQuestionnairesByUser(user.id_user);
  };

  const onClickVerProyecto = (id_project) => {
    //e.preventDefault();
    console.log("id_project = ", id_project);
    console.log("projects", projects);

    let project = projects.find(
      (selectedProject) => selectedProject.project.id_project === id_project
    );

    setSelectedProject(project);

    getProjectQuestionnaires({
      id_user: user.id_user,
      id_project: id_project,
    });
  };

  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <CssBaseline />
        <Grid container justify="center">
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <FolderIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Proyectos
            </Typography>

            <Button
              onClick={onClickNewProject}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              component={Link}
              to={"/newproject"}
            >
              Nuevo Proyecto
            </Button>
          </div>
        </Grid>
        <Grid container justify="center" spacing={4}>
          <Fragment>
            {projects
              ? projects.map((project) => (
                  <Fragment>
                    <Grid key={project.project.id_project} item>
                      {project.role === 1 ? (
                        <Fragment>
                          <Typography component="h1" variant="h5">
                            Rol Diseñador
                          </Typography>

                          <Card className={classes.root}>
                            <CardContent>
                              <Typography
                                style={{
                                  backgroundColor: "#81c784",
                                }}
                                variant="h6"
                              >
                                {project.project.title}
                              </Typography>
                              <Typography
                                className={classes.title}
                                color="textSecondary"
                                gutterBottom
                              >
                                Fecha de Creación:{" "}
                                {project.project.creation_date}
                              </Typography>
                              <Typography
                                className={classes.title}
                                color="textSecondary"
                                gutterBottom
                              >
                                Estado: {project.project.status}
                              </Typography>
                            </CardContent>
                          </Card>
                        </Fragment>
                      ) : (
                        <Fragment>
                          <Typography component="h1" variant="h5">
                            Rol Ejecutor
                          </Typography>
                          {project.role === 2 ? (
                            <Card className={classes.root}>
                              <CardContent>
                                <Typography
                                  style={{
                                    backgroundColor: "#81c784",
                                  }}
                                  variant="h6"
                                >
                                  {project.project.title}
                                </Typography>
                                <Typography
                                  className={classes.title}
                                  color="textSecondary"
                                  gutterBottom
                                >
                                  <strong>Nombre del sitio web: </strong>
                                  {project.project.website_name}
                                </Typography>
                                <Typography
                                  className={classes.title}
                                  color="textSecondary"
                                  gutterBottom
                                >
                                  <strong>Descripcion del sitio web: </strong>
                                  {project.project.website_description}
                                </Typography>
                                <Typography
                                  className={classes.title}
                                  color="textSecondary"
                                  gutterBottom
                                >
                                  <strong>Fecha de Creación: </strong>
                                  {project.project.creation_date}
                                </Typography>
                                <Typography
                                  className={classes.title}
                                  color="textSecondary"
                                  gutterBottom
                                >
                                  <strong>Estado: </strong>
                                  {project.project.status}
                                </Typography>
                              </CardContent>
                              <CardActions>
                                <Button
                                  onClick={() =>
                                    onClickVerProyecto(
                                      project.project.id_project
                                    )
                                  }
                                  component={Link}
                                  to={"/projectquestionnaires"}
                                  size="small"
                                >
                                  Ver cuestionarios
                                </Button>
                              </CardActions>
                            </Card>
                          ) : (
                            <Fragment>
                              <Typography component="h1" variant="h5">
                                Rol Revisor
                              </Typography>
                              <Card className={classes.root}>
                                <CardContent>
                                  <Typography
                                    style={{
                                      backgroundColor: "#81c784",
                                    }}
                                    variant="h6"
                                  >
                                    {project.project.title}
                                  </Typography>
                                  <Typography
                                    className={classes.title}
                                    color="textSecondary"
                                    gutterBottom
                                  >
                                    Fecha de Creación:{" "}
                                    {project.project.creation_date}
                                  </Typography>
                                  <Typography
                                    className={classes.title}
                                    color="textSecondary"
                                    gutterBottom
                                  >
                                    Estado: {project.project.status}
                                  </Typography>
                                </CardContent>
                              </Card>
                            </Fragment>
                          )}
                        </Fragment>
                      )}
                    </Grid>
                  </Fragment>
                ))
              : null}
          </Fragment>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Projects;
