import React, { useContext, useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Autocomplete from "@material-ui/lab/Autocomplete";

import Card from "@material-ui/core/Card";

import AuthContext from "../../context/auth/authContext";
import ProjectContext from "../../context/project/projectContext";
import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";

import FolderIcon from "@material-ui/icons/Folder";

import Swal from "sweetalert2";
import { Chip } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.success.light,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const NewProject = () => {
  const classes = useStyles();

  const authContext = useContext(AuthContext);
  const { user } = authContext;

  const projectContext = useContext(ProjectContext);
  const { users, createProject } = projectContext;

  const questionnaireContext = useContext(QuestionnaireContext);
  const { questionnaires_by_user } = questionnaireContext;

  const [formData, setFormData] = useState({
    title: "",
    website_name: "",
    website_url: "",
    website_description: "",
    website_justification: "",
    participants_list: [],
    questionnaires_list: [],
  });

  const [participants_list, setParticipantsList] = useState([]);

  const [questionnaires_list, setQuestionnairesList] = useState([]);

  //const [selectedQuestionnaire, setSelectedQuestionnaire] = useState(null);
  const selectedQuestionnaire = null;

  //const [selectedUser, setSelectedUser] = useState(null);
  const selectedUser = null;

  useEffect(() => {
    //eslint-disable-next-line
  }, [participants_list, formData, questionnaires_list]);

  const handleTituloProyecto = (e) => {
    const auxFormData = { ...formData };
    auxFormData.title = e.target.value;
    auxFormData.id_user = user.id_user;
    setFormData(auxFormData);
  };

  const handleNombreSitioWebProyecto = (e) => {
    const auxFormData = { ...formData };
    auxFormData.website_name = e.target.value;
    setFormData(auxFormData);
  };

  const handleURLSitioWebProyecto = (e) => {
    const auxFormData = { ...formData };
    auxFormData.website_url = e.target.value;
    setFormData(auxFormData);
  };

  const handleDescripcionSitioWebProyecto = (e) => {
    const auxFormData = { ...formData };
    auxFormData.website_description = e.target.value;
    setFormData(auxFormData);
  };

  const handleJustificacionSitioWebProyecto = (e) => {
    const auxFormData = { ...formData };
    auxFormData.website_justification = e.target.value;
    setFormData(auxFormData);
  };

  const deleteUser = (participantToDelete) => () => {
    setParticipantsList((participants) =>
      participants.filter(
        (participant) => participant.email !== participantToDelete.email
      )
    );
  };

  const deleteQuestionnaire = (questionnaireToDelete) => () => {
    setQuestionnairesList((questionnaires) =>
      questionnaires.filter(
        (questionnaire) =>
          questionnaire.id_questionnaire !==
          questionnaireToDelete.id_questionnaire
      )
    );
  };

  const handleChangeSelectedUser = (event, newVal) => {
    if (newVal) {
      const u = {
        first_name: newVal.first_name,
        last_name: newVal.last_name,
        email: newVal.email,
        executor_role: 1,
        revisor_role: 0,
      };

      let existsUser = participants_list.find(
        (userToFind) => userToFind.email === u.email
      );

      if (!existsUser) {
        const pl = [...participants_list];
        pl.push(u);
        setParticipantsList(pl);

        let fdAux = { ...formData };
        fdAux.participants_list.push(u);
        setFormData(fdAux);
      }
    } else {
      setParticipantsList(null);
    }
  };

  const handleChangeSelectedQuestionnaire = (event, newVal) => {
    if (newVal) {
      const q = {
        id_questionnaire: newVal.id_questionnaire,
        name: newVal.name,
      };
      let existsQuestionnaire = questionnaires_list.find(
        (questionnaireToFind) =>
          questionnaireToFind.id_questionnaire === q.id_questionnaire
      );
      if (!existsQuestionnaire) {
        const ql = [...questionnaires_list];
        ql.push(q);
        setQuestionnairesList(ql);
        let fdAux = { ...formData };
        fdAux.questionnaires_list.push(q);
        setFormData(fdAux);
      }
    } else {
      setQuestionnairesList(null);
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();
    console.log("form data en onSubmit", formData);
    formData.questionnaires_list = questionnaires_list;
    formData.participants_list = participants_list;
    if (
      formData.title === "" ||
      formData.website_name === "" ||
      formData.website_url === "" ||
      formData.website_description === "" ||
      formData.website_justification === "" ||
      formData.participants_list.length === 0 ||
      formData.questionnaires_list.length === 0
    ) {
      Swal.fire({
        title: "Error",
        text: "Por favor, ingrese todos los campos",
        icon: "error",
      });
    } else if (formData.title.trim().length > 100) {
      Swal.fire({
        title: "Error",
        text: "Ingresar un título de máximo 100 caracteres",
        icon: "error",
      });
    } else if (formData.website_name.trim().length > 100) {
      Swal.fire({
        title: "Error",
        text: "Ingresar un nombre del sitio web de máximo 100 caracteres",
        icon: "error",
      });
    } else if (formData.website_url.trim().length > 100) {
      Swal.fire({
        title: "Error",
        text: "Ingresar una URL del sitio web de máximo 100 caracteres",
        icon: "error",
      });
    } else if (formData.website_description.trim().length > 500) {
      Swal.fire({
        title: "Error",
        text: "Ingresar una descripción del sitio web de máximo 500 caracteres",
        icon: "error",
      });
    } else if (formData.website_justification.trim().length > 500) {
      Swal.fire({
        title: "Error",
        text:
          "Ingresar una justificación del sitio web de máximo 500 caracteres",
        icon: "error",
      });
    } else {
      createProject(formData);
    }
  };

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <FolderIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Crear Nuevo Proyecto
        </Typography>
        <form onSubmit={onSubmit} className={classes.form} noValidate>
          <TextField
            onChange={handleTituloProyecto}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="title"
            label="Título del proyecto"
            autoFocus
          />
          <TextField
            onChange={handleNombreSitioWebProyecto}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="website_name"
            label="Nombre del sitio web"
          />
          <TextField
            onChange={handleURLSitioWebProyecto}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="website_url"
            label="URL del sitio web"
          />
          <TextField
            onChange={handleDescripcionSitioWebProyecto}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="website_description"
            label="Información general del sitio web"
          />
          <TextField
            onChange={handleJustificacionSitioWebProyecto}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="website_justification"
            label="Justificación de evaluación del sitio web"
          />
          <Autocomplete
            style={{ marginTop: 15 }}
            id="projectUser"
            fullWidth
            value={selectedUser ? selectedUser : null}
            onChange={handleChangeSelectedUser}
            options={users ? users : null}
            getOptionLabel={(option) =>
              option.first_name + " " + option.last_name + " - " + option.email
            }
            renderInput={(params) => (
              <TextField
                {...params}
                label="Seleccionar usuarios del cuestionario"
                variant="outlined"
              />
            )}
          />

          <Card style={{ margin: 20 }}>
            {participants_list
              ? participants_list.map((user, userIndex) => (
                  <Chip
                    style={{ margin: 10 }}
                    label={user.first_name + " " + user.last_name}
                    variant="outlined"
                    color="primary"
                    onDelete={deleteUser(user)}
                  />
                ))
              : null}
          </Card>

          <Autocomplete
            id="projectQuestionnaire"
            value={selectedQuestionnaire ? selectedQuestionnaire : null}
            onChange={(e, selectedQuestionnaire) =>
              handleChangeSelectedQuestionnaire(e, selectedQuestionnaire)
            }
            options={questionnaires_by_user ? questionnaires_by_user : null}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Seleccionar cuestionario de usabilidad"
                variant="outlined"
              />
            )}
            fullWidth
          />

          <Card style={{ margin: 20 }}>
            {questionnaires_list
              ? questionnaires_list.map((questionnaire, questionnaireIndex) => (
                  <Chip
                    style={{ margin: 10 }}
                    label={questionnaire.name}
                    variant="outlined"
                    color="primary"
                    onDelete={deleteQuestionnaire(questionnaire)}
                  />
                ))
              : null}
          </Card>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Crear Proyecto
          </Button>
        </form>
      </div>
    </Container>
  );
};

export default NewProject;
