import React, { Fragment, useContext, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Link } from "react-router-dom";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";

import ProjectContext from "../../context/project/projectContext";
import AuthContext from "../../context/auth/authContext";
import QuestionnaireContext from "../../context/questionnaire/questionnaireContext";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const ProjectQuestionnaires = () => {
  const classes = useStyles();

  const projectContext = useContext(ProjectContext);
  const authContext = useContext(AuthContext);
  const questionnaireContext = useContext(QuestionnaireContext);

  const {
    project_questionnaires,
    id_project,
    selectedProject,
  } = projectContext;
  const { user } = authContext;

  const { getQuestionnaireToAnswer } = questionnaireContext;

  const onClickResponderCuestionario = (id_questionnaire) => {
    console.log("llamando a responder cuestionario");
    console.log("id_user", user.id_user);
    console.log("id_project", id_project);
    console.log("id_questionnaire", id_questionnaire);

    getQuestionnaireToAnswer({
      id_user: user.id_user,
      id_project: id_project,
      id_questionnaire: id_questionnaire,
    });
  };

  const onClickComentarCuestionario = (id_questionnaire) => {};

  useEffect(() => {
    console.log("selectedProject", selectedProject);
  }, [selectedProject]);

  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <CssBaseline />
        <Grid container justify="center">
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Cuestionarios del Proyecto
            </Typography>
            <div style={{ margin: 20 }}>
              <Typography variant="subtitle1">
                <strong>Título del proyecto: </strong>
                {selectedProject ? selectedProject.title : null}
              </Typography>
              <Typography variant="subtitle1">
                <strong>Nombre del sitio web: </strong>
                {selectedProject ? selectedProject.website_name : null}
              </Typography>
              <Typography variant="subtitle1">
                <strong>URL del sitio web: </strong>
                {selectedProject ? selectedProject.website_url : null}
              </Typography>
              <Typography variant="subtitle1">
                <strong>Descripción del sitio web: </strong>
                {selectedProject ? selectedProject.website_description : null}
              </Typography>
              <Typography variant="subtitle1">
                <strong>Justificación de elección del sitio web: </strong>
                {selectedProject ? selectedProject.website_justification : null}
              </Typography>
            </div>
          </div>
        </Grid>
        <Grid container justify="center" spacing={4}>
          <Fragment>
            {project_questionnaires
              ? project_questionnaires.map((questionnaire) => (
                  <Grid key={questionnaire.id_questionnaire} item>
                    <Card className={classes.root}>
                      <CardContent>
                        <Typography variant="h6">
                          {questionnaire.name}
                        </Typography>
                        <Typography
                          className={classes.title}
                          color="textSecondary"
                          gutterBottom
                        >
                          Fecha de Creación: {questionnaire.creation_date}
                        </Typography>
                        <Typography
                          className={classes.title}
                          color="textSecondary"
                          gutterBottom
                        >
                          Puntaje:{" "}
                          {questionnaire.score ? questionnaire.score : "-"}
                        </Typography>
                        <Typography
                          className={classes.title}
                          color="textSecondary"
                          gutterBottom
                        >
                          Estado:{" "}
                          {questionnaire.status
                            ? questionnaire.status
                            : "Incompleto"}
                        </Typography>
                      </CardContent>
                      <CardActions>
                        {questionnaire.status !== "Completo" ? (
                          <Button
                            onClick={() =>
                              onClickResponderCuestionario(
                                questionnaire.id_questionnaire
                              )
                            }
                            component={Link}
                            to={"/answerquestionnaire"}
                            size="small"
                          >
                            Responder Cuestionario
                          </Button>
                        ) : (
                          <Button
                            onClick={() =>
                              onClickComentarCuestionario(
                                questionnaire.id_questionnaire
                              )
                            }
                            component={Link}
                            to={"/commentquestionnaire"}
                            size="small"
                          >
                            Comentar cuestionario
                          </Button>
                        )}
                      </CardActions>
                    </Card>
                  </Grid>
                ))
              : null}
          </Fragment>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ProjectQuestionnaires;
