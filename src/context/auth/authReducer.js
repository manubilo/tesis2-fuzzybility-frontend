import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    USER_LOADED,
    LOGOUT
} from '../types'

export default (state, action) => {
    switch (action.type) {
        case USER_LOADED:
            return{
                ...state,
                isAuthenticated : true,
                user : JSON.parse(action.payload)
            }
        case REGISTER_SUCCESS:
        case LOGIN_SUCCESS:
            localStorage.setItem('user', JSON.stringify(action.payload));
            return{
                ...state,
                ...action.payload,
                isAuthenticated : true,
                user: action.payload
            }
        case REGISTER_FAIL:
        case LOGIN_FAIL:
        case LOGOUT:
            localStorage.removeItem('user')
            return{
                ...state,
                isAuthenticated : false,
                user: null,
                error: action.payload
            }
        default:
            return state;
    }
}