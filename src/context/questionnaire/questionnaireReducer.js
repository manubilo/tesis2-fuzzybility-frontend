import {
  QUESTIONNAIRE_ID,
  QUESTIONNAIRE_ID_LOADED,
  QUESTIONNAIRE_CREATION_SUCCESS,
  QUESTIONNAIRE_CREATION_FAIL,
  QUESTIONNAIRE_LIST_SUCCESS,
  QUESTIONNAIRE_LIST_FAIL,
  QUESTIONNAIRE_ANSWER_SUCCESS,
  QUESTIONNAIRE_ANSWER_FAIL,
  QUESTIONNAIRE_COMMENT_SUCCESS,
  QUESTIONNAIRE_COMMENT_FAIL,
  QUESTIONNAIRE_LIST_BY_USER_SUCCESS,
  QUESTIONNAIRE_LIST_BY_USER_FAIL,
  QUESTIONNAIRE_LIST_LOADED,
  QUESTIONNAIRE_TO_ANSWER_SUCCESS,
  QUESTIONNAIRE_TO_ANSWER_FAIL,
} from "../types";

export default (state, action) => {
  switch (action.type) {
    case QUESTIONNAIRE_ID:
      localStorage.setItem("id_questionnaire", JSON.stringify(action.payload));
      return {
        ...state,
        ...action.payload,
        id_questionnaire: action.payload,
      };
    case QUESTIONNAIRE_ID_LOADED:
      return {
        ...state,
        id_questionnaire: JSON.parse(action.payload),
      };
    case QUESTIONNAIRE_LIST_LOADED:
      return {
        ...state,
        questionnaires_by_user: JSON.parse(action.payload),
      };
    case QUESTIONNAIRE_CREATION_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case QUESTIONNAIRE_CREATION_FAIL:
      return {
        ...state,
        questionnaire_list: null,
        error: action.payload,
      };
    case QUESTIONNAIRE_LIST_SUCCESS:
      return {
        ...state,
        ...action.payload,
        questionnaires: action.payload,
      };
    case QUESTIONNAIRE_LIST_FAIL:
      return {
        ...state,
        error: action.payload,
        questionnaires: null,
      };
    case QUESTIONNAIRE_LIST_BY_USER_SUCCESS:
      localStorage.setItem(
        "questionnaires_by_user",
        JSON.stringify(action.payload)
      );
      return {
        ...state,
        ...action.payload,
        questionnaires_by_user: action.payload,
      };
    case QUESTIONNAIRE_LIST_BY_USER_FAIL:
      return {
        ...state,
        error: action.payload,
        questionnaires_by_user: null,
      };
    case QUESTIONNAIRE_ANSWER_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case QUESTIONNAIRE_ANSWER_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    case QUESTIONNAIRE_TO_ANSWER_SUCCESS:
      localStorage.setItem(
        "questionnaire_to_answer",
        JSON.stringify(action.payload)
      );
      return {
        ...state,
        ...action.payload,
        questionnaire_to_answer: action.payload,
      };
    case QUESTIONNAIRE_TO_ANSWER_FAIL:
      return {
        ...state,
        error: action.payload,
        questionnaire_to_answer: null,
      };
    case QUESTIONNAIRE_COMMENT_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case QUESTIONNAIRE_COMMENT_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
