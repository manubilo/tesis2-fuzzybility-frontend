import React, { useReducer, useEffect } from 'react';
import questionnaireReducer from './questionnaireReducer';
import QuestionnaireContext from './questionnaireContext';
import axios from 'axios';
import baseURL from '../../baseURL';
import {
  QUESTIONNAIRE_ID,
  QUESTIONNAIRE_ID_LOADED,
  QUESTIONNAIRE_CREATION_SUCCESS,
  QUESTIONNAIRE_LIST_SUCCESS,
  QUESTIONNAIRE_LIST_FAIL,
  QUESTIONNAIRE_ANSWER_SUCCESS,
  QUESTIONNAIRE_ANSWER_FAIL,
  QUESTIONNAIRE_COMMENT_SUCCESS,
  QUESTIONNAIRE_COMMENT_FAIL,
  QUESTIONNAIRE_LIST_BY_USER_SUCCESS,
  QUESTIONNAIRE_LIST_BY_USER_FAIL,
  QUESTIONNAIRE_LIST_LOADED,
  QUESTIONNAIRE_TO_ANSWER_SUCCESS,
  QUESTIONNAIRE_TO_ANSWER_FAIL,
} from '../types';

import Swal from 'sweetalert2';

const QuestionnaireState = (props) => {
  //const baseURL = 'http://52.4.175.1:5000';
  //const baseURL = "http://127.0.0.1:5000";

  const initialState = {
    questionnaires: null,
    questionnaires_by_user: null,
    questions_list: null,
    id_questionnaire: null,
    error: null,
  };

  const [state, dispatch] = useReducer(questionnaireReducer, initialState);

  useEffect(() => {
    const questionnaires_by_user = localStorage.getItem(
      'questionnaires_by_user'
    );
    if (questionnaires_by_user) {
      dispatch({
        type: QUESTIONNAIRE_LIST_LOADED,
        payload: questionnaires_by_user,
      });
    }

    const id_questionnaire = localStorage.getItem('id_questionnaire');
    if (id_questionnaire) {
      dispatch({
        type: QUESTIONNAIRE_ID_LOADED,
        payload: id_questionnaire,
      });
    }
    //eslint-disable-next-line
  }, []);

  const createQuestionnaire = async (formData, subattribute_list) => {
    formData.subattribute_list = subattribute_list;
    console.log('Creando cuestionario', formData);

    const URL = baseURL + '/api/questionnaire/create';
    axios(URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: formData,
    })
      .then((res) => {
        dispatch({
          type: QUESTIONNAIRE_CREATION_SUCCESS,
          payload: res.data,
        });
        Swal.fire({
          title: 'Éxito',
          text: 'Cuestionario creado exitosamente',
          icon: 'success',
        });
        console.log('response', res.data);
      })
      .catch((error) => {
        Swal.fire({
          title: 'Error',
          text: 'El cuestionario no se pudo crear',
          icon: 'success',
        });
        throw error;
      });
  };

  const listQuestionnaires = async (formData) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const res = axios.post(
      baseURL + '/api/questionnaire/listQuestionnaires',
      formData,
      config
    );

    if (res.data.code >= 200 && res.data.code < 300) {
      dispatch({
        type: QUESTIONNAIRE_LIST_SUCCESS,
        payload: res.data,
      });
    } else {
      console.log('Questionnaire list failed');
      dispatch({
        type: QUESTIONNAIRE_LIST_FAIL,
        payload: 'Questionnaire list failed',
      });
      Swal.fire({
        title: 'Error',
        text: 'No se pudieron obtener sus cuestionarios ',
        icon: 'error',
      });
    }
  };

  const listQuestionnairesByUser = async (formData) => {
    const id_user = formData;

    formData = {
      id_user: id_user,
    };

    console.log('formData = ', formData);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(
        baseURL + '/api/questionnaire/listQuestionnairesByUser',
        formData,
        config
      );
      if (res.data.code >= 200 && res.data.code < 300) {
        console.log('list questionnaires', res.data);
        dispatch({
          type: QUESTIONNAIRE_LIST_BY_USER_SUCCESS,
          payload: res.data.body.questionnaires_list,
        });
      }
    } catch (err) {
      dispatch({
        type: QUESTIONNAIRE_LIST_BY_USER_FAIL,
        payload: 'Questionnaires by user list failed',
      });
      Swal.fire({
        title: 'Error',
        text: 'Sus cuestionarios no se pudieron obtener',
        icon: 'error',
      });
    }
  };

  const getQuestionnaireToAnswer = async (formData) => {
    console.log('getting questions list', formData);
    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const res = await axios.post(
      baseURL + '/api/questionnaire/listQuestions',
      formData,
      config
    );

    console.log(res.data);

    if (res.data.code >= 200 && res.data.code < 300) {
      dispatch({
        type: QUESTIONNAIRE_TO_ANSWER_SUCCESS,
        payload: res.data.body.subattribute_list,
      });
      dispatch({
        type: QUESTIONNAIRE_ID,
        payload: res.data.body.id_questionnaire,
      });
    } else {
      console.log('Questions list failed');
      dispatch({
        type: QUESTIONNAIRE_TO_ANSWER_FAIL,
        payload: 'Questions list failed',
      });
      Swal.fire({
        title: 'Error',
        text: 'Las preguntas del cuestionario no pudo ser obtenido',
        icon: 'error',
      });
    }
  };

  const answerQuestionnaire = async (formData) => {
    console.log('Respondiendo cuestionario');
    console.log('estoy enviando esto formData', formData);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(
        baseURL + '/api/questionnaire/answer',
        formData,
        config
      );

      console.log('Respuesta API', res.data);

      if (res.data.code >= 200 && res.data.code < 300) {
        dispatch({
          type: QUESTIONNAIRE_ANSWER_SUCCESS,
          payload: res.data,
        });
        Swal.fire({
          title: 'Éxito',
          text: 'Cuestionario respondido exitosamente',
          icon: 'success',
        });
      }
    } catch (err) {
      console.log('Questionnaire answer failed');
      dispatch({
        type: QUESTIONNAIRE_ANSWER_FAIL,
        payload: 'Questionnaire answer failed',
      });
      Swal.fire({
        title: 'Error',
        text: 'El cuestionario no pudo ser respondido',
        icon: 'error',
      });
    }
  };

  const commentQuestionnaire = async (formData) => {
    console.log('Respondiendo comentarios de cuestionario');

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(
        baseURL + '/api/questionnaire/comment',
        formData,
        config
      );

      console.log('Respuesta API', res.data);

      if (res.data.code >= 200 && res.data.code < 300) {
        dispatch({
          type: QUESTIONNAIRE_COMMENT_SUCCESS,
          payload: res.data,
        });
        Swal.fire({
          title: 'Éxito',
          text: 'Los comentarios fueron guardados exitosamente',
          icon: 'success',
        });
      }
    } catch (err) {
      console.log('Questionnaire comment failed');
      dispatch({
        type: QUESTIONNAIRE_COMMENT_FAIL,
        payload: 'Questionnaire comment failed',
      });
      Swal.fire({
        text: 'Los comentarios del cuestionario no pudieron ser respondidos',
        icon: 'error',
      });
    }
  };

  return (
    <QuestionnaireContext.Provider
      value={{
        id_questionnaire: state.id_questionnaire,
        questionnaires: state.questionnaires,
        questionnaires_by_user: state.questionnaires_by_user,
        questionnaire_to_answer: state.questionnaire_to_answer,
        createQuestionnaire,
        listQuestionnaires,
        listQuestionnairesByUser,
        getQuestionnaireToAnswer,
        answerQuestionnaire,
        commentQuestionnaire,
      }}
    >
      {props.children}
    </QuestionnaireContext.Provider>
  );
};

export default QuestionnaireState;
