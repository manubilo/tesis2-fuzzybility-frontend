import {
  PROJECT_ID,
  PROJECT_ID_LOADED,
  PROJECT_CREATION_SUCCESS,
  PROJECT_CREATION_FAIL,
  PROJECT_LIST_SUCCESS,
  PROJECT_LIST_FAIL,
  PROJECT_GET_USERS_SUCCESS,
  PROJECT_GET_USERS_FAIL,
  PROJECT_USERS_LOADED,
  PROJECT_QUESTIONNAIRES_SUCCESS,
  PROJECT_QUESTIONNAIRES_FAIL,
  PROJECT_SET_SELECTED_PROJECT,
} from "../types";

export default (state, action) => {
  switch (action.type) {
    case PROJECT_ID:
      localStorage.setItem("id_project", JSON.stringify(action.payload));
      return {
        ...state,
        ...action.payload,
        id_project: action.payload,
      };
    case PROJECT_ID_LOADED:
      return {
        ...state,
        id_project: JSON.parse(action.payload),
      };
    case PROJECT_USERS_LOADED:
      return {
        ...state,
        users: JSON.parse(action.payload),
      };
    case PROJECT_CREATION_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case PROJECT_CREATION_FAIL:
      return {
        ...state,
        projects: null,
        error: action.payload,
      };
    case PROJECT_LIST_SUCCESS:
      return {
        ...state,
        ...action.payload,
        projects: action.payload,
      };
    case PROJECT_LIST_FAIL:
      return {
        ...state,
        error: action.payload,
        projects: null,
      };
    case PROJECT_GET_USERS_SUCCESS:
      localStorage.setItem("users", JSON.stringify(action.payload));
      return {
        ...state,
        ...action.payload,
        users: action.payload,
      };
    case PROJECT_GET_USERS_FAIL:
      return {
        ...state,
        users: null,
        error: action.payload,
      };
    case PROJECT_QUESTIONNAIRES_SUCCESS:
      localStorage.setItem(
        "project_questionnaires",
        JSON.stringify(action.payload)
      );
      return {
        ...state,
        ...action.payload,
        project_questionnaires: action.payload,
      };
    case PROJECT_QUESTIONNAIRES_FAIL:
      return { ...state, project_questionnaires: null };
    case PROJECT_SET_SELECTED_PROJECT:
      return { ...state, selectedProject: action.payload };
    default:
      return state;
  }
};
