import React, { useReducer, useEffect } from 'react';
import projectReducer from './projectReducer';
import ProjectContext from './projectContext';
import axios from 'axios';
import baseURL from '../../baseURL';
import {
  PROJECT_ID,
  PROJECT_ID_LOADED,
  PROJECT_CREATION_SUCCESS,
  PROJECT_CREATION_FAIL,
  PROJECT_LIST_SUCCESS,
  PROJECT_LIST_FAIL,
  PROJECT_GET_USERS_SUCCESS,
  PROJECT_GET_USERS_FAIL,
  PROJECT_USERS_LOADED,
  PROJECT_QUESTIONNAIRES_SUCCESS,
  PROJECT_QUESTIONNAIRES_FAIL,
  PROJECT_SET_SELECTED_PROJECT,
} from '../types';

import Swal from 'sweetalert2';

const ProjectState = (props) => {
  //const baseURL = "http://52.4.175.1:5000";
  //const baseURL = "http://127.0.0.1:5000";

  const initialState = {
    projects: null,
    project_questionnaires: null,
    id_project: null,
    users: null,
    error: null,
    selectedProject: null,
  };

  const [state, dispatch] = useReducer(projectReducer, initialState);

  useEffect(() => {
    const users = localStorage.getItem('users');
    if (users) {
      dispatch({
        type: PROJECT_USERS_LOADED,
        payload: users,
      });

      const id_project = localStorage.getItem('id_project');
      if (id_project) {
        console.log('id_project_loaded');
        dispatch({
          type: PROJECT_ID_LOADED,
          payload: id_project,
        });
      }
    }
    //eslint-disable-next-line
  }, []);

  const createProject = async (formData) => {
    console.log('Crear proyecto', formData);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(
        baseURL + '/api/project/create',
        formData,
        config
      );

      console.log('Create project success', res.data);

      if (res.data.code >= 200 && res.data.code < 300) {
        dispatch({
          type: PROJECT_CREATION_SUCCESS,
          payload: res.data,
        });
        Swal.fire({
          title: 'Éxito',
          text: 'Proyecto creado exitosamente',
          icon: 'success',
        });
      }
    } catch (err) {
      console.log('Create project failed');
      dispatch({
        type: PROJECT_CREATION_FAIL,
        payload: 'Fallo la creacion de cuestionario',
      });
      Swal.fire({
        title: 'Error',
        text: 'No se pudo crear el proyecto',
        icon: 'error',
      });
    }
  };

  const listProjects = async (formData) => {
    console.log('List projects', formData);
    const id_user = formData;

    formData = {
      id_user: id_user,
    };

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(
        baseURL + '/api/project/list',
        formData,
        config
      );
      console.log('Respuesta API', res.data);

      if (res.data.code >= 200 && res.data.code < 300) {
        dispatch({
          type: PROJECT_LIST_SUCCESS,
          payload: res.data.body.project_list,
        });
      }
    } catch (err) {
      console.log('Project list failed');
      dispatch({
        type: PROJECT_LIST_FAIL,
        payload: 'Project list failed',
      });
      Swal.fire({
        title: 'Error',
        text: 'Sus proyectos no se pudieron obtener',
        icon: 'error',
      });
    }
  };

  const getUsers = async () => {
    console.log('Get users');

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(baseURL + '/api/user/getall', config);

      console.log('Respuesta API', res.data);

      if (res.data.code >= 200 && res.data.code < 300) {
        dispatch({
          type: PROJECT_GET_USERS_SUCCESS,
          payload: res.data.body.user_list,
        });
      }
    } catch (err) {
      console.log('Get all users failed');
      dispatch({
        type: PROJECT_GET_USERS_FAIL,
        payload: 'Get all users failed',
      });
      Swal.fire({
        title: 'Error',
        text: 'No se pudieron obtener los usuarios',
        icon: 'error',
      });
    }
  };

  const getProjectQuestionnaires = async (formData) => {
    console.log('getting project questionnaires', formData);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(
        baseURL + '/api/questionnaire/listQuestionnaires',
        formData,
        config
      );

      if (res.data.code >= 200 && res.data.code < 300) {
        dispatch({
          type: PROJECT_QUESTIONNAIRES_SUCCESS,
          payload: res.data.body.questionnaires_list,
        });
        dispatch({
          type: PROJECT_ID,
          payload: res.data.body.id_project,
        });
      } else {
        console.log('Get all users failed');
        dispatch({
          type: PROJECT_QUESTIONNAIRES_FAIL,
          payload: 'Get all project questionnaires failed',
        });
        Swal.fire({
          title: 'Error',
          text: 'No se pudieron obtener los cuestionarios del proyecto',
          icon: 'error',
        });
      }
    } catch (err) {}
  };

  const setSelectedProject = (project) => {
    dispatch({
      type: PROJECT_SET_SELECTED_PROJECT,
      payload: project.project,
    });
  };

  return (
    <ProjectContext.Provider
      value={{
        projects: state.projects,
        project_questionnaires: state.project_questionnaires,
        users: state.users,
        id_project: state.id_project,
        error: state.error,
        selectedProject: state.selectedProject,
        createProject,
        listProjects,
        getUsers,
        getProjectQuestionnaires,
        setSelectedProject,
      }}
    >
      {props.children}
    </ProjectContext.Provider>
  );
};

export default ProjectState;
