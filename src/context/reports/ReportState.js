import React, { useReducer, useEffect } from 'react';
import reportReducer from './reportReducer';
import ReportContext from './reportContext';
import axios from 'axios';
import baseURL from '../../baseURL';
import {
  REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_SUCCESS,
  REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_FAIL,
  REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_LOADED,
  REPORT_GET_ANSWERS_SUCCESS,
  REPORT_GET_ANSWERS_FAIL,
} from '../types';

import Swal from 'sweetalert2';

const ReportState = (props) => {
  //const baseURL = 'http://52.4.175.1:5000';
  //const baseURL = "http://127.0.0.1:5000";

  const initialState = {
    reports_questionnaires_by_project: null,
    answers: null,
    subfactos: null,
  };

  const [state, dispatch] = useReducer(reportReducer, initialState);

  useEffect(() => {
    const reports_questionnaires_by_project = localStorage.getItem(
      'reports_questionnaires_by_project'
    );
    if (reports_questionnaires_by_project) {
      dispatch({
        type: REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_LOADED,
        payload: reports_questionnaires_by_project,
      });
    }
    //eslint-disable-next-line
  }, []);

  const listQuestionnairesByProject = async (formData) => {
    console.log('Obteniendo los cuestionarios del proyecto');

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const res = await axios.post(
        baseURL + '/api/reports/listProjectsWithQuestionnaires',
        formData,
        config
      );

      console.log('Respuesta API', res.data);

      if (res.data.code >= 200 && res.data.code < 300) {
        dispatch({
          type: REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_SUCCESS,
          payload: res.data.body.questionnaires_report_list,
        });
      }
    } catch (err) {
      console.log('List questionnaires by project for reports failed');
      dispatch({
        type: REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_FAIL,
        payload: 'List questionnaires by project for reports failed',
      });
      Swal.fire({
        text: 'No se pudo obtener los cuestionarios del proyecto',
        icon: 'error',
      });
    }
  };

  const getAnswers = async (formData) => {
    console.log('Obteniendo las respuestas del cuestionario');
    console.log('formData', formData);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const res = await axios.post(
      baseURL + '/api/questionnaire/getAnswers',
      formData,
      config
    );

    console.log('Respuesta API', res.data);

    if (res.data) {
      if (res.data.code >= 200 && res.data.code < 300) {
        console.log('getAnswers success');
        dispatch({
          type: REPORT_GET_ANSWERS_SUCCESS,
          payload: res.data.body.answers,
        });
      } else {
        console.log('List answers failed');
        dispatch({
          type: REPORT_GET_ANSWERS_FAIL,
          payload: 'List answers failed',
        });
        Swal.fire({
          text: 'No se pudo obtener las respuestas de los cuestionarios',
          icon: 'error',
        });
      }
    }
  };

  return (
    <ReportContext.Provider
      value={{
        reports_questionnaires_by_project:
          state.reports_questionnaires_by_project,
        answers: state.answers,
        subfactors: state.subfactors,
        listQuestionnairesByProject,
        getAnswers,
      }}
    >
      {props.children}
    </ReportContext.Provider>
  );
};

export default ReportState;
