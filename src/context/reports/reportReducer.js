import {
  REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_SUCCESS,
  REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_FAIL,
  REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_LOADED,
  REPORT_GET_ANSWERS_SUCCESS,
  REPORT_GET_ANSWERS_FAIL,
} from "../types";

export default (state, action) => {
  switch (action.type) {
    case REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_LOADED:
      return {
        ...state,
        reports_questionnaires_by_project: JSON.parse(action.payload),
      };
    case REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_SUCCESS:
      localStorage.setItem(
        "reports_questionnaires_by_project",
        JSON.stringify(action.payload)
      );
      return {
        ...state,
        ...action.payload,
        reports_questionnaires_by_project: action.payload,
      };
    case REPORT_LIST_QUESTIONNAIRES_BY_PROJECT_FAIL:
      return {
        ...state,
        reports_questionnaires_by_project: null,
      };
    case REPORT_GET_ANSWERS_SUCCESS:
      console.log("entre a REPORT_GET_ANSWERS_SUCCESS");
      return {
        ...state,
        answers: action.payload.factors,
        subfactors: action.payload.subfactors,
      };
    case REPORT_GET_ANSWERS_FAIL:
      return {
        ...state,
        answers: null,
        subfactors: null,
      };
    default:
      return state;
  }
};
