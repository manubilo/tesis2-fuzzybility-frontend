import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/layout/Navbar";
import Projects from "./components/projects/Projects";
import ProjectQuestionnaires from "./components/projects/ProjectQuestionnaires";
import AnswerQuestionnaire from "./components/questionnaires/AnswerQuestionnaire";
import NewProject from "./components/projects/NewProject";
import Questionnaires from "./components/questionnaires/Questionnaires";
import NewQuestionnaire from "./components/questionnaires/NewQuestionnaire";
import Report from "./components/reports/Report";
import SignIn from "./components/auth/SignIn";
import SignUp from "./components/auth/SignUp";
import About from "./components/layout/About";
import ReportsChart from "./components/reports/ReportsChart";
import CommentQuestionnaire from "./components/questionnaires/CommentQuestionnaire";

import AuthState from "./context/auth/AuthState";
import ProjectState from "./context/project/ProjectState";
import QuestionnaireState from "./context/questionnaire/QuestionnaireState";
import ReportState from "./context/reports/ReportState";

import "./App.css";
import Landing from "./components/layout/Landing";

const App = () => {
  return (
    <AuthState>
      <ProjectState>
        <QuestionnaireState>
          <ReportState>
            <Router>
              <div className="App">
                <Navbar />
                <div>
                  <Switch>
                    <Route exact path="/" component={Landing} />
                    <Route exact path="/about" component={About} />
                    <Route exact path="/signin" component={SignIn} />
                    <Route exact path="/signup" component={SignUp} />
                    <Route exact path="/projects" component={Projects} />
                    <Route
                      exact
                      path="/projectquestionnaires"
                      component={ProjectQuestionnaires}
                    />
                    <Route
                      exact
                      path="/answerquestionnaire"
                      component={AnswerQuestionnaire}
                    />
                    <Route exact path="/newproject" component={NewProject} />
                    <Route
                      exact
                      path="/questionnaires"
                      component={Questionnaires}
                    />
                    <Route
                      exact
                      path="/newquestionnaire"
                      component={NewQuestionnaire}
                    />
                    <Route exact path="/reports" component={Report} />
                    <Route
                      exact
                      path="/reportsChart"
                      component={ReportsChart}
                    />
                    <Route
                      exact
                      path="/commentquestionnaire"
                      component={CommentQuestionnaire}
                    />
                  </Switch>
                </div>
              </div>
            </Router>
          </ReportState>
        </QuestionnaireState>
      </ProjectState>
    </AuthState>
  );
};

export default App;
